import { createRoot } from 'react-dom/client';
import axios from 'axios';
import { MutationCache, Query, QueryCache, QueryClient, QueryClientProvider } from 'react-query';
import { ReactQueryDevtools } from 'react-query/devtools';
import Moment from 'react-moment';
import toast, { Toaster } from 'react-hot-toast';

Moment.globalFormat = 'MMM D, YYYY';
Moment.globalParse = 'YYYY-MM-DDTHH:mm:ss.SSS' as string;

const queryClient = new QueryClient({
  queryCache: new QueryCache({
    onError: (error: any, query: Query) => {
      const status = error.response?.status;
      if (status == 401) {
        toast.error(`Please Authenticate.`);
      }
      if (status == 403) {
        toast.error(
          `You do not have permission to view this page.  Please contact your organization admin.`
        );
        console.error(error);
      }
      //  only show error toasts if we already have data in the cache which indicates a failed background update
      else if (query.state.data !== undefined) {
        toast.error(`Something went wrong, please try your request again.`);
      }
      console.error(error);
    },
  }),
  mutationCache: new MutationCache({
    onError: (error: any) => {
      const status = error.response?.status;
      if (status !== 422) {
        toast.error(`Something went wrong, please try your request again.`);
        console.error(error);
      }
    },
  }),
  defaultOptions: {
    queries: {
      // SEE: https://tanstack.com/query/v4/docs/guides/window-focus-refetching
      refetchOnWindowFocus: false,

      // SEE: https://tanstack.com/query/v4/docs/guides/suspense
      suspense: true,
    },
  },
});
const container = document.getElementById('root');
if (container) {
  createRoot(container).render(
    <QueryClientProvider client={queryClient}>
      <Toaster
        position='bottom-center'
        reverseOrder={false}
        gutter={24}
        containerClassName=''
        containerStyle={{margin: '0 0 10px 0'}}
        toastOptions={{
          className: '',
          duration: 3000,
          style: {
            color: '#fff',
            animation: 'custom-exit 1s ease',
            transition: 'all 0.5s ease-out',
          },
        }}
      />
      <ReactQueryDevtools initialIsOpen={false}/>
    </QueryClientProvider>
  );
}
